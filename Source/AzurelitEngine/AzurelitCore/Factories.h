#pragma once

namespace AzurelitFactories
{
	namespace JobFactory
	{
		// Simple redirection to MakeStrongPointer for now. Should probably do more?
		template<typename ClassInstance, typename... ConstructionArguments>
		static StrongPointer<ClassInstance> MakeJob(ConstructionArguments... args)
		{
			return ManagedPointers::MakeStrongPointer<ClassInstance, ConstructionArguments...>(std::forward<ConstructionArguments>(args)...);
		}
	}
}