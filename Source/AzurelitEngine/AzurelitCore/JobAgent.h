#pragma once
#include <Common\Utils\DataStructures\Queue\Queue.h>
#include <Common\Utils\Pointers\Pointers.h>
#include <AzurelitEngine\AzurelitCore\JobSystem\Jobs\AJob.h>
#include <Common\Utils\Threading\Thread.h>
#include <Common\Utils\Events\Event.h>

#include <stdlib.h>

class JobAgent
{
public:
	JobAgent();
	~JobAgent();

	int id = 0;

	JobAgent(const JobAgent& other)
	{
		thread = other.thread;

		running = other.running;

		softJobCap = other.softJobCap;
		hardJobCap = other.hardJobCap;

		jobQueue = other.jobQueue;

		id = other.id;
	}

	JobAgent(JobAgent&& other)
	{
		other;
	}


	void Run()
	{
		if (!IsRunning())
		{
			running = true;
			thread.BindFunction<JobAgent, &JobAgent::ThreadLogic>(this);
		}
	}

	void Stop()
	{
		running = false;
	}

	void AddJobToQueue(StrongPointer<AJob>& job)
	{
		jobQueue.Push(std::move(job));

		if (!IsRunning())
		{
			Run();
		}
	}

	bool IsRunning() const
	{
		return thread.ThreadID() != 0 && running;
	}


	bool IsFree() const
	{
		return jobQueue.Count() < softJobCap;
	}

	bool HasSpace() const
	{
		return jobQueue.Count() < hardJobCap;
	}

	bool operator<(const JobAgent& other) const
	{
		return JobCount() < other.JobCount();
	}

	Event<JobAgent&> OnAgentStartedJob;
	Event<JobAgent&> OnAgentWaitingNextJob;
	Event<JobAgent&> OnAgentFinishedJob;

private:

	void ThreadLogic()
	{
		while (running)
		{
			if (jobQueue.Count() > 0)
			{
				StrongPointer<AJob> currentJob = jobQueue.Front();
				if (currentJob.IsValid())
				{
					jobQueue.Front()->ExecuteJob();
				}
				jobQueue.Pop();
			}
			else
			{
				running = false;
			}
		}
	}

	unsigned int JobCount() const
	{
		return jobQueue.Count();
	}

private:
	AzurelitUtils::Thread thread;

	bool running = false;

	unsigned int softJobCap = 10;
	unsigned int hardJobCap = 20;

	AzurelitUtils::Queue<StrongPointer<AJob>> jobQueue;
};

