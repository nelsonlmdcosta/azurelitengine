#pragma once

#include <Common\Utils\Singleton\ASingleton.h>

class AssetManager : public ASingleton<AssetManager>
{
public:
	AssetManager();
	~AssetManager();

};

