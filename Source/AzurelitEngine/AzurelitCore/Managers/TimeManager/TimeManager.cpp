#include "TimeManager.h"

TimeManager::TimeManager()
{
}
TimeManager::~TimeManager()
{
	if ( canReadCpu )
	{
		PdhCloseQuery( queryHandle );
	}
}

bool TimeManager::Initialize()
{
	if(!InitializeTimer())
	{
		return false;
	}

	if (!InitializeFPSCounter())
	{
		return false;
	}

	if(!InitializeCPUCounter())
	{
		return false;
	}

	return true;
}
bool TimeManager::InitializeTimer()
{
	// Check to see if this system supports high performance timers.
	QueryPerformanceFrequency(reinterpret_cast<LARGE_INTEGER*>(&frequency));
	if (frequency == 0)
	{
		return false;
	}

	// Find out how many times the frequency counter ticks every millisecond.
	ticksPerMs = static_cast<float>(frequency / 1000);

	QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&startTime));

	return true;
}
bool TimeManager::InitializeFPSCounter()
{
	fpsStartTime = timeGetTime();

	return true;
}
bool TimeManager::InitializeCPUCounter()
{
	PDH_STATUS status;

	canReadCpu = true;

	// Create a query object to poll cpu usage.
	status = PdhOpenQuery(nullptr, 0, &queryHandle);
	if (status != ERROR_SUCCESS)
	{
		canReadCpu = false;
	}

	// Set query object to poll all cpus in the system.
	status = PdhAddCounter(queryHandle, TEXT("\\Processor(_Total)\\% processor time"), 0, &counterHandle);
	if (status != ERROR_SUCCESS)
	{
		canReadCpu = false;
	}

	lastSampleTime = GetTickCount64();

	cpuUsage = 0;

	return true;
}

void TimeManager::Update()
{
	UpdateTime();
	UpdateFPS();
	UpdateCPU();
}
void TimeManager::UpdateTime()
{
	INT64 currentTime;
	float timeDifference;

	QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&currentTime));

	timeDifference = static_cast<float>(currentTime - startTime);

	frameTime = (timeDifference / ticksPerMs) / 1000;
	runTime += frameTime;

	startTime = currentTime;
}
void TimeManager::UpdateFPS()
{
	++frameCounter;

	if (timeGetTime() >= (fpsStartTime+ 1000))
	{
		fps = frameCounter;
		frameCounter = 0;

		fpsStartTime = timeGetTime();
	}
}
void TimeManager::UpdateCPU()
{
	PDH_FMT_COUNTERVALUE value;

	if (canReadCpu)
	{
		if ((lastSampleTime + 1000) < GetTickCount64())
		{
			lastSampleTime = GetTickCount64();

			PdhCollectQueryData(queryHandle);

			PdhGetFormattedCounterValue(counterHandle, PDH_FMT_LONG, nullptr, &value);

			cpuUsage = value.longValue;
		}
	}
}

float TimeManager::DeltaTime() const
{
	return frameTime;
}
float TimeManager::RunTime() const
{
	return runTime;
}
unsigned int TimeManager::FPSCounter() const
{
	return fps;
}
unsigned int TimeManager::CPUUsage() const
{
	return canReadCpu ? static_cast<int>(cpuUsage) : 0;
}
