/*
Description:
Class That Handles Timer 

TODO: Implemented as a windows only class, we will need to make this a virtual probably so we have multiple types of windows

Author:   Nelson Luis Moreira da Costa
Email:    nelsonlmdcosta@gmail.com
LinkedIn: http://www.linkedin.com/in/nelsonlmdcosta
*/

#pragma once

#pragma comment(lib, "pdh.lib")
#pragma comment(lib, "winmm.lib")

#include <pdh.h>
#include <windows.h>

#include <Common\Utils\Singleton\ASingleton.h>

class TimeManager : public ASingleton<TimeManager>
{
public:
	TimeManager();
	~TimeManager();

	bool Initialize();
	void Update();

	void AddNewTimer();

	float DeltaTime() const;
	float RunTime() const;
	// Scene Time?


	unsigned int FPSCounter() const;
	unsigned int CPUUsage() const;

	// Add a timed job system?

private:
	// Time Variables
	INT64 frequency		= 0;
	float ticksPerMs	= 0;

	INT64 startTime		= 0;

	float frameTime		= 0;
	float runTime		= 0;

	// FPS Variables
	unsigned int	fps				= 0;
	unsigned int	frameCounter	= 0;
	unsigned long	fpsStartTime	= 0;

	// CPU Variables
	bool		canReadCpu		= false;
	HQUERY		queryHandle		= nullptr;
	HCOUNTER	counterHandle	= nullptr;
	ULONGLONG	lastSampleTime	= 0;
	long		cpuUsage		= 0;

	// Functions
	bool InitializeTimer();
	bool InitializeFPSCounter();
	bool InitializeCPUCounter();

	void UpdateTime();
	void UpdateFPS();
	void UpdateCPU();
};
