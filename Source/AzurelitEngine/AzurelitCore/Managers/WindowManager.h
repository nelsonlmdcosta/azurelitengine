/*
Author:   Nelson Luis Moreira da Costa
Email:    nelsonlmdcosta@gmail.com
LinkedIn: http://www.linkedin.com/in/nelsonlmdcosta

Description:
Object To Get Main And Any Auxiliary Window
*/

#pragma once

#include <memory>
#include <Common\Utils\Singleton\ASingleton.h>
#include <Common\Utils\Pointers\Pointers.h>

class AWindow;

// TODO: Destroy all of this, it's unnecesary?
class WindowManager : public ASingleton<WindowManager>
{
public:
	WindowManager();
	~WindowManager();

	bool Initialize();
	void Update();
	void Terminate();

	template<class T>
	WeakPointer<T> GetMainWindowAs() 
	{
		return mainWindow.As<T>();
	}

	WeakPointer<AWindow> GetMainWindow() { return mainWindow; }

private:
	// TODO: This should be a factory, this class should be responsible for the memory but not for its creation
	bool CreateMainApplicationWindow();

	StrongPointer<AWindow> mainWindow;
};

