// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "WindowManager.h"

#include <AzurelitEngine\AzurelitCore\Window\AWindow.h>
#include <AzurelitEngine\AzurelitCore\Window\MS\MSWindow.h>


WindowManager::WindowManager()
{
}


WindowManager::~WindowManager()
{
}

bool WindowManager::Initialize()
{
	if (!CreateMainApplicationWindow())
	{
		return false;
	}

	return true;
}

void WindowManager::Update()
{
	if (mainWindow.IsValid())
	{
		mainWindow->Update();
	}
}

void WindowManager::Terminate()
{
	if (mainWindow.IsValid())
	{
		mainWindow->Terminate();
		mainWindow.Clear();
	}
}

bool WindowManager::CreateMainApplicationWindow()
{
	mainWindow = ManagedPointers::MakeStrongPointer<MSWindow>().As<AWindow>();

	if (mainWindow.IsValid() && mainWindow->Initialize())
	{
		return true;
	}

	// Some log message
	return false;
}