/*
Author:   Nelson Luis Moreira da Costa
Email:    nelsonlmdcosta@gmail.com
LinkedIn: http://www.linkedin.com/in/nelsonlmdcosta

Brief:
File used to hold onto global variables for debugging purposes mostly, I'm sure this will be abused at some point
*/
#pragma once

#include <Common\Utils\Singleton\ASingleton.h>
#include <Common\Utils\DataStructures\Stack\Stack.h>
#include <AzurelitEngine\AzurelitCore\Scene\AScene.h>
#include <Common\Utils\DataStructures\Queue\Queue.h>
#include <Common\Utils\Pointers\Pointers.h>

#include <AzurelitEngine\AzurelitCore\JobSystem\Jobs\AJob.h>
#include <AzurelitEngine\AzurelitCore\JobSystem\Jobs\SceneLoadingJob.h>
#include <Common\Utils\DataStructures\ArrayList\ArrayList.h>

class SceneArguments{};

class SceneManager : public ASingleton<SceneManager>
{
public:
	SceneManager();
	~SceneManager();

	// Top Most Scene Of The Stack
	WeakPointer<AScene> GetActiveScene();

	void Update();

	void LoadSceneSynced(int hashedKey);
	void LoadSceneUnsynced(int hashedKey);

	void UnloadScene(int hashedKey);

	// Pass In A Scene To Load And Play Immediately
	void PlayScene(int hashedKey, SceneArguments& sceneArguments);

	// Stack of scenes, only the topmost one is executed
	AzurelitUtils::Stack<StrongPointer<AScene>> sceneStack;

	// List Of Ready Scenes That Are Already Loaded ( Normally Just Loaded, Or Popped Off The Stack )
	AzurelitUtils::UnsortedArrayList<StrongPointer<AScene>> unkilledScenes;

	// Scenes To Load (Jobs)
	AzurelitUtils::SortedArrayList<StrongPointer<AJob>> loadingQueue;
};

