#include "JobSystem.h"
#include <Common\Utils\Events\Event.h>
#include <AzurelitEngine\AzurelitCore\CommandlineArguments.h>



JobSystem::JobSystem()
{
}

JobSystem::~JobSystem()
{
}

bool JobSystem::Initialize()
{
	// Defaults to 10 Threads this should change depending on platform and available cores/hyperthreads
	int jobAgentTotal = 10;
	if (CommandlineArguments::HasArgument("OverrideTotalJobAgents"))
	{
		jobAgentTotal = 15;
	}

	for (int i = 0; i < jobAgentTotal; i++)
	{
		JobAgent& newAgent = jobAgents.AddNewElement();

		newAgent.OnAgentStartedJob.Subscribe<JobSystem, &JobSystem::OnAgentStartedJobCallback>(this);
		newAgent.OnAgentWaitingNextJob.Subscribe<JobSystem, &JobSystem::OnAgentWaitingJobCallback>(this);
		newAgent.OnAgentFinishedJob.Subscribe<JobSystem, &JobSystem::OnAgentFinishedJobCallback>(this);

		newAgent.id = i;
	}

	return true;
}

void JobSystem::Terminate()
{
	// Wait Till All jobs Done Then Join And Close
}
