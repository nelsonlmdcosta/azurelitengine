/*
Author:   Nelson Luis Moreira da Costa
Email:    nelsonlmdcosta@gmail.com
LinkedIn: http://www.linkedin.com/in/nelsonlmdcosta

Brief:
File used to hold onto global variables for debugging purposes mostly, I'm sure this will be abused at some point

TODO:
- If def it out for release builds?
*/

#pragma once

#include <Common\Utils\Delegate\Delegate.h>

enum class EJobPriority : int
{
	Low,
	Normal,
	High,
	Urgent,
	Immediate,
};

class AJob
{
public:
	AJob() 
		: isRunning(false)
		, isFinished(false)
		, jobPriority(EJobPriority::Normal)
	{}
	
	virtual ~AJob() 
	{}

	void ExecuteJob()
	{
		isRunning = true;
		
		RunJob();

		isRunning = false;
		isFinished = true;
	}

	bool IsRunning() { return isRunning; }
	bool IsFinished() { return isFinished; }

	Delegate<void> OnStartedCallback;
	Delegate<void> OnFinishedCallback;

	bool operator<(const AJob& rhs)
	{
		return jobPriority < rhs.jobPriority;
	}

protected:

	virtual void RunJob() = 0;

private:
	bool isRunning = false;
	bool isFinished = false;

	EJobPriority jobPriority;
};

