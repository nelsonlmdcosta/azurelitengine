#pragma once

#include <AzurelitEngine\AzurelitCore\JobSystem\Jobs\AJob.h>
#include <Common\Utils\Logging\AzurelitLogger.h>
#include <Common\Utils\Threading\Locks\ScopedThreadLock.h>


class SceneLoadingJobData
{
public:
	SceneLoadingJobData(int i) { someTestInt = i; }
	SceneLoadingJobData(const SceneLoadingJobData& other) = default;

	int someTestInt = 0;

	ThreadLock internalDataLock;
};

class SceneLoadingJob : public AJob
{
public:
	SceneLoadingJob() = delete;
	SceneLoadingJob(SceneLoadingJobData& data);
	~SceneLoadingJob();

	virtual void RunJob() override
	{
		int iterations = 10; // 1000000000;
		for (int i = 0; i < iterations; i++)
		{
			//ScopedThreadLock(internalData.internalDataLock);
			internalData.someTestInt += i;
		}
		AzurelitLogger::LogMessage("Job Done: " + std::to_string(internalData.someTestInt));
	}

private:

	SceneLoadingJobData& internalData;
};

