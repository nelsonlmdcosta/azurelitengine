/*
Author:   Nelson Luis Moreira da Costa
Email:    nelsonlmdcosta@gmail.com
LinkedIn: http://www.linkedin.com/in/nelsonlmdcosta

Brief:
File used to hold onto global variables for debugging purposes mostly, I'm sure this will be abused at some point

TODO:
- If def it out for release builds?
*/

#pragma once

#include <Common\Utils\Singleton\ASingleton.h>

#include <AzurelitEngine\AzurelitCore\JobSystem\Jobs\AJob.h>

#include <Common\Utils\DataStructures\Stack\Stack.h>

#include <Common\Utils\Threading\Thread.h>
#include <Common\Utils\DataStructures\Queue\Queue.h>
#include <Common\Utils\Pointers\Pointers.h>
#include <AzurelitEngine\AzurelitCore\Factories.h>
#include <Common\Utils\DataStructures\ArrayList\ArrayList.h>
#include <AzurelitEngine\AzurelitCore\JobAgent.h>

class JobSystem : public ASingleton<JobSystem>
{
public:

	JobSystem();
	~JobSystem();

	JobSystem(const JobSystem& other) = delete;
	JobSystem(JobSystem&& other) = delete;

	JobSystem& operator=(const JobSystem& other) = delete;
	JobSystem& operator=(JobSystem&& other) = delete;

	bool Initialize();
	void Terminate();

public:
	template<typename JobType, typename... JobArguments>
	StrongPointer<AJob> QueueNewJob(JobArguments... jobArguments)
	{
		// Weird syntax is due to using the dependant template
		StrongPointer<AJob> newJob = AzurelitFactories::JobFactory::template MakeJob<JobType, JobArguments...>(std::forward<JobArguments>(jobArguments)...).As<AJob>();

		jobPriorityQueue.Push(newJob);

		DispatchJobsToAvailableAgents();

		return newJob;
	}



private:
	void RemoveJob();

	void DispatchJobsToAvailableAgents()
	{
		PrioritizeLowestJobCountAgent();

		//RoundRobinOnBelowSoftCappedAgents();

		//RoundRobinOnBelowHardCappedAgents();
	}

	void RoundRobinOnBelowSoftCappedAgents()
	{
		bool depositedJob = true;
		while (depositedJob == true)
		{
			depositedJob = false;

			// Deposit Jobs in the mailbox in the queue robin fashion via highest priority
			for (unsigned int i = 0; i < jobAgents.Count(); ++i)
			{
				JobAgent& agent = jobAgents[i];
				if (agent.IsFree() && jobPriorityQueue.Count() != 0)
				{
					StrongPointer<AJob> job = jobPriorityQueue.Top();
					agent.AddJobToQueue(job);
					jobPriorityQueue.Pop();
					depositedJob = true;
				}

				if (jobPriorityQueue.Count() == 0)
				{
					// Set this back to false to force ourselves out of the while loop
					depositedJob = false;
					break;
				}
			}
		}
	}

	void RoundRobinOnBelowHardCappedAgents()
	{
		bool depositedJob = true;
		while (depositedJob == true)
		{
			depositedJob = false;

			// Deposit Jobs in the mailbox in the queue robin fashion via highest priority
			for (unsigned int i = 0; i < jobAgents.Count(); ++i)
			{
				JobAgent& agent = jobAgents[i];
				if (agent.HasSpace() && jobPriorityQueue.Count() != 0)
				{
					StrongPointer<AJob> job = jobPriorityQueue.Top();
					agent.AddJobToQueue(job);
					jobPriorityQueue.Pop();
					depositedJob = true;
				}

				if (jobPriorityQueue.Count() == 0)
				{
					// Set this back to false to force ourselves out of the while loop
					depositedJob = false;
					break;
				}
			}
		}
	}

	void PrioritizeLowestJobCountAgent()
	{
		bool dispatchedJob = true;
		while (dispatchedJob)
		{
			JobAgent& lowestJobCountAgent = FindLowestJobCountAgent();

			if (lowestJobCountAgent.HasSpace() && jobPriorityQueue.Count())
			{
				StrongPointer<AJob> job = jobPriorityQueue.Top();
				lowestJobCountAgent.AddJobToQueue(job);
				jobPriorityQueue.Pop();
				dispatchedJob = true;
			}
			else
			{
				dispatchedJob = false;
			}
		}
	}

	JobAgent& FindLowestJobCountAgent()
	{
		int index = 0;
		for (unsigned int i = 1; i < jobAgents.Count(); ++i)
		{
			if (jobAgents[i] < jobAgents[i - 1])
			{
				index = i;
			}
		}

		return jobAgents[index];
	}

	void FillJobsOnAgent(JobAgent& agent)
	{
		while (agent.IsFree())
		{
			StrongPointer<AJob> job = jobPriorityQueue.Top();
			agent.AddJobToQueue(job);
			jobPriorityQueue.Pop();
		}
	}

	void OnAgentStartedJobCallback(JobAgent& agent)
	{
		// Do I Care You Started?
		agent;
	}

	void OnAgentWaitingJobCallback(JobAgent& agent)
	{
		if (HasPendingJobs())
		{
			FillJobsOnAgent(agent);
		}
	}

	void OnAgentFinishedJobCallback(JobAgent& agent)
	{
		if (HasPendingJobs())
		{
			FillJobsOnAgent(agent);
		}
	}

	bool HasPendingJobs()
	{
		return jobPriorityQueue.Count() != 0;
	}


private:
	AzurelitUtils::PriorityQueue<StrongPointer<AJob>> jobPriorityQueue;

	AzurelitUtils::UnsortedArrayList<JobAgent> jobAgents;
};
