#pragma once
#include <Common\Utils\DataStructures\ArrayList\ArrayList.h>
#include <string>

/*
	Nifty little class that allows for us to parse and store command line arguments into the application and use them.
	Note: These are NOT case insensitive atm, might be worth doing that at soem point

	List of current known ones:
	PauseOnTerminate - Pauses application when we terminate basically a _getch at the end so we can read any logs in the console window

*/

class CommandlineArguments
{
public:
	friend class AApplication;

	CommandlineArguments() = delete;
	~CommandlineArguments() = delete;
	
	CommandlineArguments(CommandlineArguments&& other) = delete;
	CommandlineArguments& operator=(CommandlineArguments&& other) = delete;
	
	CommandlineArguments(const CommandlineArguments& other) = delete;
	CommandlineArguments& operator=(const CommandlineArguments& other) = delete;

	static bool HasArgument(std::string argument)
	{
		for (unsigned int i = 0; i < commandlineArguments.Count(); ++i)
		{
			std::string currentArgument = commandlineArguments[i];
			if (strcmp(commandlineArguments[i].c_str(), argument.c_str()) == 0)
			{
				return true;
			}
		}
		return false;
	}
	
private:

	static void AddArgument(std::string& argument)
	{
		commandlineArguments.AddNewElement(argument);
	}

	static void AddArgument(std::string argument)
	{
		commandlineArguments.AddNewElement(argument);
	}

private:

	// TODO: Upgrade this once we have the HashedString Object Running
	static AzurelitUtils::UnsortedArrayList<std::string> commandlineArguments;

};