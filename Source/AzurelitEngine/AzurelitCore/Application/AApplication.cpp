// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "AApplication.h"


#include <iostream>
#include <AzurelitEngine\AzurelitCore\EngineConstants.h>

// TODO: Delete These After We Add Something Into Those Projects
#include <AzurelitEngine\AudioEngine\AudioTest.h>
#include <AzurelitEngine\PhysicsEngine\PhysicsTest.h>

#include <Common\Utils\Logging\AzurelitLogger.h>
#include <AzurelitEngine\AzurelitCore\Managers\TimeManager\TimeManager.h>
#include <AzurelitEngine\AzurelitCore\Managers\WindowManager\WindowManager.h>
#include <AzurelitEngine\AzurelitCore\Window\AWindow.h>
#include <Common\Utils\Pointers\Pointers.h>
#include <conio.h>
#include <AzurelitEngine\GraphicsEngine\GraphicsFactory.h>
#include <Common\ThirdParty\ThirdPartyTest.h>
#include <Common\Utils\UtilsTest.h>
#include <AzurelitEngine\AzurelitCore\CommandlineArguments.h>
#include <AzurelitEngine\AzurelitCore\Managers\SceneManager\SceneManager.h>
#include <AzurelitEngine\AzurelitCore\JobSystem\JobSystem.h>


//

AApplication::AApplication()
	: isApplicationRunning(true)
{
	// This is jsut here to force the compilation and linking
	AudioTest at;
	at.SomeSuperFunction();

	PhysicsTest pt;
	pt.SomeSuperFunction();

	ThirdPartyTest tpt;
	tpt.SomeSuperFunction();

	UtilsTest ut;
	ut.SomeSuperFunction();

}

AApplication::~AApplication()
{

}

bool AApplication::IsApplicationRunning()
{
	return isApplicationRunning;
}

void AApplication::RequestApplicationTermination()
{
	isApplicationRunning = false;
}

bool AApplication::Initialize() 
{ 
	// Initialize Engine Systems
	if (!TimeManager::Instance().Initialize())
	{
		AzurelitLogger::LogCriticalError("AApplication::Initialize: Time Manager Failed To Initialize");
		return false;
	}

	if (!JobSystem::Instance().Initialize())
	{
		AzurelitLogger::LogCriticalError("AApplication::Initialize: Job System Failed To Initialize");
		return false;
	}

	// Initialize Required Systems
	if (WindowManager::Instance().Initialize())
	{
		WeakPointer<AWindow> someWindow = WindowManager::Instance().GetMainWindow();
		if (someWindow.IsValid())
		{
			someWindow->OnWindowCloseEvent.Subscribe<AApplication, &AApplication::OnWindowClosedCallback>(this);
		}
	}
	else
	{
		AzurelitLogger::LogCriticalError("AApplication::Initialize: Window Manager Failed To Initialize");
		return false;
	}

	sceneRenderer = GraphicsFactory::CreateGraphicsRenderer();
	if (sceneRenderer.IsValid())
	{
		if (sceneRenderer->Initialize())
		{
			// Yay?
		}
	}


	// Initialize Application Game Systems
	InitializeSystems();

	return true;
}

void AApplication::Terminate() 
{
	TerminateSystems();
}

void AApplication::Run() 
{
	static int frameNumber = 0;

	while (isApplicationRunning)
	{
		float deltaTime = 0;

		TimeManager::Instance().Update();
		deltaTime = TimeManager::Instance().DeltaTime();

		WindowManager::Instance().Update();

		SceneManager::Instance().Update();

		sceneRenderer->RenderActiveScenes();
	}
}

void AApplication::OnWindowClosedCallback()
{
	RequestApplicationTermination();
}

void AApplication::ParseCommandLineParameters(int argumentCount, char* arguments[])
{
	// Ignore the first one as it's the application's cwd
	for (int i = 1; i < argumentCount; ++i)
	{
		CommandlineArguments::AddArgument(arguments[i]);
	}
}

#pragma region

int main(int argc, char* argv[])
{
	if (AzurelitLogger::InitializeLogger())
	{
		if (AApplication::Instance().IsValid())
		{
			AzurelitLogger::LogMessage("Parsing Command Line Parameters");
			AApplication::Instance()->ParseCommandLineParameters(argc, argv);

			AzurelitLogger::LogMessage("Initializing Application");
			if (AApplication::Instance()->Initialize())
			{
				AzurelitLogger::LogMessage("Running Application");
				AApplication::Instance()->Run();
			}

			AzurelitLogger::LogMessage("Terminating Application");
			AApplication::Instance()->Terminate();
		}
	}
	else
	{
		return -1;
	}

	if (CommandlineArguments::HasArgument("PauseOnTerminate"))
	{
		AzurelitLogger::LogMessage("Application Termination Pause Requested");

		_getch();
	}

	return 0;
}

#pragma endregion EntryPoints