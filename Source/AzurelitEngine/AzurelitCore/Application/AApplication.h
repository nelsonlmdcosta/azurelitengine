/*
Author:   Nelson Luis Moreira da Costa
Email:    nelsonlmdcosta@gmail.com
LinkedIn: http://www.linkedin.com/in/nelsonlmdcosta

Brief:
Abstraction of an application, this class handles all the intialization and basic logic to get the application running

TODO:
Change this to a DLL instead? Might be cleaner way as it has access to a main
*/

#pragma once

#include <Common\Utils\Pointers\Pointers.h>
#include <Common\Utils\Singleton\ASingleton.h>
#include <AzurelitEngine\GraphicsEngine\Renderer\ISceneRenderer.h>

class AApplication
{
public:
	friend int main(int argc, char* argv[]);

	AApplication();
	virtual ~AApplication();

	AApplication(const AApplication& other) = delete;
	AApplication(const AApplication&& other) = delete;

	AApplication& operator=(const AApplication& other) = delete;
	AApplication& operator=(const AApplication&& other) = delete;

	bool IsApplicationRunning();
	void RequestApplicationTermination();

	static WeakPointer<AApplication> Instance() { return applicationInstance; }

private: // Only friends should have access to these

	bool Initialize();
	void Terminate();
	void Run();

	void ParseCommandLineParameters(int argumentCount, char* arguments[]);

	void OnWindowClosedCallback();

protected: // Functions extended by the application

	virtual bool InitializeSystems() = 0;
	virtual void TerminateSystems() = 0;
	

	static StrongPointer<AApplication> applicationInstance;

	StrongPointer<ISceneRenderer> sceneRenderer;

private:

	bool isApplicationRunning;

};

