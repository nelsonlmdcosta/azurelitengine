/*
Author:   Nelson Luis Moreira da Costa
Email:    nelsonlmdcosta@gmail.com
LinkedIn: http://www.linkedin.com/in/nelsonlmdcosta

Description:
Base class to show a window
*/

#pragma once

#include <AzurelitEngine\AzurelitCore\Window\AWindow.h>

class MSWindow : public AWindow
{
public:
	friend ::LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam);

	MSWindow();
	~MSWindow();

	virtual bool Initialize() override;
	virtual void Update() override;
	virtual void Terminate() override;

	HWND GetWindowHandle() { return windowHandle; }

protected:
	virtual bool LoadSettings() override;
	virtual bool SaveSettings() override;

	void InitializeAndRegisterWindowClass();
	void InitializeDevModeScreenSettings();

	// TODO: Change To A Calculate Instead Returning The LONG
	long CalculateWindowStyle();
	bool SetWindowStyle(long windowStyle);

	bool CreateApplicationWindow();


private: 
	// These Are Specific to this file so lets keep this here
	HWND windowHandle				= nullptr;
	HINSTANCE windowInstanceHandle	= nullptr;

	// TODO: Hand This Over TO The TimeManager ?
	float targetTime = 0.0f;

private:

	void DisplayApplicationMetrics();

};

