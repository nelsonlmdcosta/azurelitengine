// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "MSWindow.h"

#include <fstream>
#include <sstream>

#include <AzurelitEngine\AzurelitCore\Managers\TimeManager\TimeManager.h>
#include <AzurelitEngine\AzurelitCore\Managers\WindowManager\WindowManager.h>

#include <Common\ThirdParty\RapidXML\rapidxml.hpp>
#include <Common\ThirdParty\RapidXML\rapidxml_print.hpp>
#include <Common\Utils\Logging\AzurelitLogger.h>
#include <Common\Utils\Pointers\Pointers.h>


// TODO: Add Microsoft Pages Comments


MSWindow::MSWindow()
{

}

MSWindow::~MSWindow()
{
	SaveSettings();

	UnregisterClass(windowSettings.WindowName().c_str(), windowInstanceHandle);
}

bool MSWindow::Initialize()
{
	OnWindowPreInitializeEvent.BroadCast();

	if (!LoadSettings())
	{
		AzurelitLogger::LogCriticalError("Microsoft Window Failed To Load Settings");
		return false;
	}

	// Get Instance Handler
	windowInstanceHandle = GetModuleHandle(nullptr);

	InitializeAndRegisterWindowClass();
	InitializeDevModeScreenSettings();

	if (!CreateApplicationWindow())
	{
		AzurelitLogger::LogCriticalError("Microsoft Window Failed Create Window");
		return false;
	}

	if (!SetWindowStyle(CalculateWindowStyle()))
	{
		//__LINE__
		AzurelitLogger::LogCriticalError("Microsoft Window Failed To Set Style");
	}

	// Show The Window On The Foreground And Set It To Be Focused
	InvalidateRect(windowHandle, nullptr, true);
	UpdateWindow(windowHandle);

	ShowWindow(windowHandle, SW_SHOW);

	SetForegroundWindow(windowHandle);
	SetFocus(windowHandle);

	ShowCursor(windowSettings.CursorState());

	OnWindowPostInitializeEvent.BroadCast();

	return true;
}

void MSWindow::Update()
{
	OnWindowPreUpdateEvent.BroadCast();

	// Only Show/Update Metrics If We Have A Border And Not Fullscreen
	if (!windowSettings.Fullscreen() && !windowSettings.Borderless())
	{
		DisplayApplicationMetrics();
	}

	// Prepare message Object To Get Information From
	MSG message;
	ZeroMemory(&message, sizeof(MSG));

	// Update Specific Window
	// Completely Unnecesary, This Only Wasted CPU Cycles And Spammed The App With Far Too Many WM_Paint Messages
	//UpdateWindow(windowHandle);

	// Handle all the queued window messages.
	while (PeekMessage(&message, windowHandle, 0, 0, PM_REMOVE))
	{
		// Translate and dispatch the message
		TranslateMessage(&message);
		DispatchMessage(&message);

		// Clear The Data Structure
		ZeroMemory(&message, sizeof(MSG));
	}

	OnWindowPostUpdateEvent.BroadCast();
}

void MSWindow::Terminate()
{
	OnWindowPreTerminateEvent.BroadCast();

	OnWindowPostTerminateEvent.BroadCast();
}

#pragma region

bool MSWindow::LoadSettings()
{
	// Read XML Data
	rapidxml::xml_document<> doc;
	rapidxml::xml_node<>* rootNode;
	rapidxml::xml_node<>* tempNode;

	std::ifstream file("../../Config/Config.xml");
	if (file.fail())
		return false;

	std::vector<char> buffer((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
	buffer.push_back('\0');
	doc.parse<0>(&buffer[0]);

	file.close();

	// Get Application Name From Root Node
	rootNode = doc.first_node("Window");
	if (rootNode)
	{
		windowSettings.WindowName(rootNode->first_attribute("name")->value());
		//strcpy_s(windowSettings.windowName, rootNode->first_attribute("name")->value_size() + 1, rootNode->first_attribute("name")->value());
	}

	// Get FullScreen
	tempNode = rootNode->first_node("FullScreen");
	if (tempNode)
	{
		windowSettings.Fullscreen
		(
			atoi(tempNode->first_attribute("state")->value()) != 0 ? true : false
		);
	}

	// Get Borderless
	tempNode = rootNode->first_node("Borderless");
	if (tempNode)
	{
		windowSettings.Borderless
		(
			atoi(tempNode->first_attribute("state")->value()) != 0 ? true : false
		);
	}

	// Get Window Width and Height
	tempNode = rootNode->first_node("Resolution");
	if (tempNode)
	{
		windowSettings.WindowWidth
		(
			atoi(tempNode->first_attribute("width")->value())
		);

		windowSettings.WindowHeight
		(
			atoi(tempNode->first_attribute("height")->value())
		);
	}

	// Get Cursor State
	tempNode = rootNode->first_node("Cursor");
	if (tempNode)
	{
		windowSettings.CursorState
		(
			atoi(tempNode->first_attribute("state")->value())
		);
	}

	return true;
}

bool MSWindow::SaveSettings()
{
	// Read XML Data
	rapidxml::xml_document<> doc;
	rapidxml::xml_node<>* rootNode;
	rapidxml::xml_node<>* tempNode;

	std::ifstream infile("../Config/Config.xml");
	if (infile.fail())
		return false;

	std::vector<char> xmlbuffer((std::istreambuf_iterator<char>(infile)), std::istreambuf_iterator<char>());
	xmlbuffer.push_back('\0');
	doc.parse<0>(&xmlbuffer[0]);

	infile.close();

	// Set Application Name From Root Node
	rootNode = doc.first_node("Window");
	if (rootNode)
	{
		rootNode->first_attribute("name")->value(windowSettings.WindowName().c_str());
	}

	// Set FullScreen
	tempNode = rootNode->first_node("FullScreen");
	if (tempNode)
	{
		tempNode->first_attribute("state")->value(windowSettings.Fullscreen() ? "1" : "0");
	}

	// Set FullScreen
	tempNode = rootNode->first_node("Borderless");
	if (tempNode)
	{
		tempNode->first_attribute("state")->value(windowSettings.Borderless() ? "1" : "0");
	}

	// Set Window Width and Height
	tempNode = rootNode->first_node("Resolution");
	if (tempNode)
	{
		char widthBuffer[10];
		ZeroMemory(widthBuffer, sizeof(char) * 10);
		int width = windowSettings.WindowWidth();
		_itoa_s(width, widthBuffer, 10);

		tempNode->first_attribute("width")->value(widthBuffer);

		char heightBuffer[10];
		ZeroMemory(heightBuffer, sizeof(char) * 10);
		int height = windowSettings.WindowHeight();
		_itoa_s(height, heightBuffer, 10);

		tempNode->first_attribute("height")->value(heightBuffer);
	}

	// Set Cursor State
	tempNode = rootNode->first_node("Cursor");
	if (tempNode)
	{
		char buffer[128];
		int input = 0;

		input = windowSettings.CursorState();
		_itoa_s(input, buffer, 10);

		tempNode->first_attribute("state")->value(buffer);
	}

	std::ofstream outfile("../Config/Config.xml");
	if (outfile.fail())
		return false;

	outfile << doc;

	outfile.close();

	return true;
}

#pragma endregion XML_Serialization

void MSWindow::InitializeAndRegisterWindowClass()
{
	WNDCLASSEX windowClassExtended;

	// Setup and Set Extended Windows Class 
	windowClassExtended.style = CS_HREDRAW | CS_VREDRAW;// | CS_OWNDC;
	windowClassExtended.lpfnWndProc = WndProc;
	windowClassExtended.cbClsExtra = 0;
	windowClassExtended.cbWndExtra = 0;
	windowClassExtended.hInstance = windowInstanceHandle;
	windowClassExtended.hIcon = nullptr; // (HICON)LoadImage(nullptr, "Icon.ico", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_LOADTRANSPARENT);
	windowClassExtended.hIconSm = windowClassExtended.hIcon;
	windowClassExtended.hCursor = LoadCursor(nullptr, IDC_ARROW);
	windowClassExtended.hbrBackground = static_cast<HBRUSH>(GetStockObject(BLACK_BRUSH));
	windowClassExtended.lpszMenuName = nullptr;
	windowClassExtended.lpszClassName = static_cast<LPCSTR>(windowSettings.WindowName().c_str());
	windowClassExtended.cbSize = sizeof(WNDCLASSEX);

	RegisterClassEx(&windowClassExtended);
}

void MSWindow::InitializeDevModeScreenSettings()
{
	// Get Reference To The Screen Settings And Zero Them Out
	DEVMODE windowScreenSettings;
	memset(&windowScreenSettings, 0, sizeof(windowScreenSettings));

	unsigned long screenWidth = GetSystemMetrics(SM_CXSCREEN);
	unsigned long screenHeight = GetSystemMetrics(SM_CYSCREEN);

	unsigned long windowWidth = windowSettings.WindowWidth();
	unsigned long windowHeight = windowSettings.WindowHeight();

	// Setup The Screen Settings
	windowScreenSettings.dmSize = sizeof(windowScreenSettings);
	windowScreenSettings.dmPelsWidth = windowWidth;
	windowScreenSettings.dmPelsHeight = windowHeight;
	windowScreenSettings.dmBitsPerPel = 32;
	windowScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

	// Set The Window To Be Perfectly In The Middle
	windowSettings.WindowOffsetX((screenWidth - windowWidth) / 2);
	windowSettings.WindowOffsetY((screenHeight - windowHeight) / 2);
}

long MSWindow::CalculateWindowStyle()
{
	long newWindowStyle = 0;

	if (windowSettings.Borderless())
	{
		newWindowStyle |= WS_BORDER;
	}
	else
	{
		newWindowStyle |= WS_SYSMENU;		// Button Contexts On Bar
		newWindowStyle |= WS_MINIMIZEBOX;	// Adds minimize button
		newWindowStyle |= WS_CAPTION;		// Titlebar - Includes WS_BORDER
	}

	newWindowStyle |= WS_CLIPSIBLINGS;
	newWindowStyle |= WS_CLIPCHILDREN;
	newWindowStyle |= WS_VISIBLE;
	newWindowStyle |= WS_POPUP;
	newWindowStyle |= WS_OVERLAPPED;

	return newWindowStyle;
}

// TODO: Fix FullScreen To Windowed Positioning
bool MSWindow::SetWindowStyle(long windowStyle)
{
	// Should Be The Same As Previous Offset To Be Erroneous
	// https://msdn.microsoft.com/en-us/library/windows/desktop/ms644898(v=vs.85).aspx
	if (SetWindowLongPtr(windowHandle, GWL_STYLE, windowStyle) == 0)
	{
		return false;
		// Erroneous
		// GetLastError
	}

	return true;
}

bool MSWindow::CreateApplicationWindow()
{
	LPCSTR applicationName = static_cast<LPCSTR>(windowSettings.WindowName().c_str());

	windowHandle = CreateWindow(
		applicationName,
		applicationName,
		CalculateWindowStyle(),
		windowSettings.WindowOffsetX(),
		windowSettings.WindowOffsetY(),
		windowSettings.WindowWidth(),
		windowSettings.WindowHeight(),
		NULL,
		NULL,
		windowInstanceHandle,
		NULL);

	return windowHandle ? true : false;
}






void MSWindow::DisplayApplicationMetrics()
{
	if (TimeManager::Instance().RunTime() > targetTime)
	{
		// Why one second? The set text only seems to do that so no point flooding the window with messages
		// TODO: Make the time manager take hold of this? No Need To Constantly Update THis
		targetTime = TimeManager::Instance().RunTime() + 1.0f;

		std::ostringstream stream;

		stream << windowSettings.WindowName()
			<< " - Main Thread: FPS: "
			<< std::to_string(TimeManager::Instance().FPSCounter())
			<< " | CPU: "
			<< std::to_string(TimeManager::Instance().CPUUsage())
			<< "%";

		SetWindowText(windowHandle, stream.str().c_str());
	}
}









// Default Window Callback Handler
LRESULT CALLBACK MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
{
	return DefWindowProc(hwnd, umsg, wparam, lparam);
}

// Default Window Message Calls So We Can Add Custom Functionality Before Defaulting
LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam)
{
	//Event<>	OnWindowMovedEvent;
	//Event<>	OnWindowFocusEvent;

	WeakPointer<MSWindow> mainWindow = WindowManager::Instance().GetMainWindowAs<MSWindow>();
	if (mainWindow.IsValid())
	{


		switch (umessage)
		{
		case WM_SIZING: // Disable Resizing
		{
			mainWindow->OnWindowResizeEvent.BroadCast();

			AzurelitLogger::LogMessage("WndProc: Resizing Call");

			// Message Used To Resize Window
			// Ref: https://docs.microsoft.com/en-us/windows/desktop/winmsg/wm-sizing
			break;
		}
		case WM_SIZE: // Disable Resize Window Message
		{	// Message Used To Say The Size Changed, Like Maximize, Minimize Etc
			// Ref: https://docs.microsoft.com/en-us/windows/desktop/winmsg/wm-size
			// Main Ones Being (Compare To WPARAM): 
			//		SIZE_MAXIMIZED
			//		SIZE_MINIMIZED
			//		SIZE_RESTORED
			if (wparam == SIZE_MAXIMIZED)
			{
				mainWindow->OnWindowMaximizedEvent.BroadCast();

				AzurelitLogger::LogMessage("WndProc: Maximized Call");

				return 1;
			}

			if (wparam == SIZE_MINIMIZED)
			{
				mainWindow->OnWindowMinimizedEvent.BroadCast();

				AzurelitLogger::LogMessage("WndProc: Minimized Call");

				return 1;
			}

			if (wparam == SIZE_RESTORED)
			{
				mainWindow->OnWindowRestoredEvent.BroadCast();

				AzurelitLogger::LogMessage("WndProc: Restored Call");

				return 1;
			}

			break;
		}
		case WM_MOVE:
		{
			mainWindow->OnWindowMovedEvent.BroadCast();

			WindowSettings& windowSettings = const_cast<WindowSettings&>(mainWindow->GetWindowSettings());

			windowSettings.WindowOffsetX((int)(short)LOWORD(lparam));
			windowSettings.WindowOffsetY((int)(short)HIWORD(lparam));

			AzurelitLogger::LogMessage("WndProc: Moved Call");

			return 0;
		}
		break;
		case WM_CLOSE: // Message That Someone Clicked The Close Button
		{
			// Get Main Window From Window Manager
			// Dispatch The Windows Close Event
			mainWindow->OnWindowCloseEvent.BroadCast();

			AzurelitLogger::LogMessage("WndProc: Closed Call");

			// Sent The WM_DESTROY Message To The Window
			DestroyWindow(mainWindow->GetWindowHandle());

			return 1;
		}
		case WM_DESTROY: // Message To Destroy The Window
		{
			// Posts a WM_QUIT That The Update Receives To Know When To Quit			
			AzurelitLogger::LogMessage("WndProc: Destroy Call");

			PostQuitMessage(0);

			return 1;
		}
		default:
		{
			// Default Behaviour For All The Other Messages We Don't Want To Override
			return MessageHandler(hwnd, umessage, wparam, lparam);
		}
		}
	}

	// Default Return Message
	return 0;
}
