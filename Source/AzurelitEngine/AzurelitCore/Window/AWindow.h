/*
Author:   Nelson Luis Moreira da Costa
Email:    nelsonlmdcosta@gmail.com
LinkedIn: http://www.linkedin.com/in/nelsonlmdcosta

Description:
Base class to show a window
*/

#pragma once

#include <Windows.h>

#include <Common/Utils/Events/Event.h>
#include <Common/Utils/Singleton/ASingleton.h>

#include <AzurelitEngine/AzurelitCore/Window/Settings/WindowSettings.h>

class AWindow
{
	friend LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam);

	//friend class AApplication;

public:

	AWindow() {}
	~AWindow() {}

	AWindow(const AWindow& other) = delete;
	AWindow(const AWindow&& other) = delete;
	AWindow& operator=(const AWindow& other) = delete;
	AWindow& operator=(const AWindow&& other) = delete;

	const WindowSettings& GetWindowSettings()
	{
		return windowSettings;
	}

	virtual bool Initialize() = 0;
	virtual void Update() {};
	virtual void Terminate() = 0;

	template<class T>
	T* As()
	{
		return dynamic_cast<T>(this);
	}

protected:

	virtual bool LoadSettings() = 0;
	virtual bool SaveSettings() = 0;

public:	// Events

	Event<>	OnWindowPreInitializeEvent;
	Event<>	OnWindowPostInitializeEvent;

	Event<>	OnWindowMinimizedEvent;
	Event<>	OnWindowMaximizedEvent;
	Event<>	OnWindowRestoredEvent;

	Event<>	OnWindowResizeEvent;
	Event<>	OnWindowMovedEvent;
	Event<>	OnWindowFocusEvent;

	Event<> OnWindowCloseEvent;

	Event<>	OnWindowPreUpdateEvent;
	Event<>	OnWindowPostUpdateEvent;

	Event<>	OnWindowPreTerminateEvent;
	Event<>	OnWindowPostTerminateEvent;

protected:

	WindowSettings windowSettings;

};

