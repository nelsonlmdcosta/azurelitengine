// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "WindowSettings.h"

WindowSettings::WindowSettings()
{
}

WindowSettings::~WindowSettings()
{
}

const std::string& WindowSettings::WindowName() const 
{ 
	return windowName; 
}

void WindowSettings::WindowName(const char* val) 
{ 
	windowName = val; 
}

bool WindowSettings::Fullscreen() const 
{ 
	return fullscreen; 
}

void WindowSettings::Fullscreen(bool val) 
{ 
	fullscreen = val; 
}

bool WindowSettings::Borderless() const 
{ 
	return borderless; 
}

void WindowSettings::Borderless(bool val) 
{ 
	borderless = val; 
}

int WindowSettings::WindowWidth() const 
{ 
	return windowWidth; 
}

void WindowSettings::WindowWidth(int val) 
{ 
	windowWidth = val; 
}

int WindowSettings::WindowHeight() const 
{ 
	return windowHeight; 
}

void WindowSettings::WindowHeight(int val) 
{ 
	windowHeight = val; 
}

int WindowSettings::WindowOffsetX() const 
{ 
	return windowOffsetX; 
}

void WindowSettings::WindowOffsetX(int val) 
{ 
	windowOffsetX = val; 
}

int WindowSettings::WindowOffsetY() const 
{
	return windowOffsetY; 
}

void WindowSettings::WindowOffsetY(int val) 
{
	windowOffsetY = val; 
}

int WindowSettings::CursorState() const 
{
	return cursorState; 
}

void WindowSettings::CursorState(int val) 
{
	cursorState = val; 
}