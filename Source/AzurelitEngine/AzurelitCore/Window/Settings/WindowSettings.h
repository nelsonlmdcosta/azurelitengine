/*
Author:   Nelson Luis Moreira da Costa
Email:    nelsonlmdcosta@gmail.com
LinkedIn: http://www.linkedin.com/in/nelsonlmdcosta

Description:
Base class to show a window
*/

#pragma once
#include <string>

class WindowSettings
{
public:
	
	WindowSettings();
	~WindowSettings();

	const std::string& WindowName() const;
	void WindowName(const char* val);
	
	bool Fullscreen() const;
	void Fullscreen(bool val);
	
	bool Borderless() const;
	void Borderless(bool val);
	
	int WindowWidth() const;
	void WindowWidth(int val);
	
	int WindowHeight() const;
	void WindowHeight(int val);

	int WindowOffsetX() const;
	void WindowOffsetX(int val);
	
	int WindowOffsetY() const;
	void WindowOffsetY(int val);
	
	int CursorState() const;
	void CursorState(int val);

private:
	
	std::string windowName = "Unnamed Application";

	bool fullscreen = false;
	bool borderless = false;

	int windowWidth = 1280;
	int windowHeight = 720;

	int windowOffsetX = 0;
	int windowOffsetY = 0;

	int cursorState = 0;

};

