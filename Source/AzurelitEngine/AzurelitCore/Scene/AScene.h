#pragma once
class AScene
{
public:
	AScene();
	virtual ~AScene();

	bool Initialize();
	void Terminate();

	bool LoadSceneFromFile();
	void DestroyScene();


};

