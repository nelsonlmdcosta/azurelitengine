#include "GraphicsFactory.h"

#include <Common\Utils\Pointers\Pointers.h>

#include <AzurelitEngine\GraphicsEngine\Interface\IGraphicsInterface.h>
#include <AzurelitEngine\GraphicsEngine\Interface\D3D12GraphicsInterface\D3D12GraphicsInterface.h>
#include <AzurelitEngine\GraphicsEngine\Renderer\ISceneRenderer.h>

StrongPointer<IGraphicsInterface> GraphicsFactory::CreateGraphicsInterface()
{
	// Find API Define And Create
	return ManagedPointers::MakeStrongPointer<D3D12GraphicsInterface>().As<IGraphicsInterface>();
}

StrongPointer<ISceneRenderer> GraphicsFactory::CreateGraphicsRenderer()
{
	// Needs upgrading to a specific renderer as opposed to being a interface
	return ManagedPointers::MakeStrongPointer<ISceneRenderer>();
}