#include "D3D12Renderer.h"
#include <AzurelitEngine\GraphicsEngine\GraphicsFactory.h>
#include <AzurelitEngine\AzurelitCore\Managers\SceneManager\SceneManager.h>



D3D12Renderer::D3D12Renderer()
{
}


D3D12Renderer::~D3D12Renderer()
{
}

bool D3D12Renderer::Initialize()
{
	graphicsInterface = GraphicsFactory::CreateGraphicsInterface();

	sceneManager = SceneManager::InstanceAsStrongPointer();

	return true;
}

bool D3D12Renderer::RenderActiveScenes()
{
	// Get The Scene Manager Singleton And Get The Currently Active Stack of Levels

	return true;
}

void D3D12Renderer::Terminate()
{

	graphicsInterface.Clear();
}
