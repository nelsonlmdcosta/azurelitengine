#pragma once

#include <AzurelitEngine/GraphicsEngine/Renderer/ISceneRenderer.h>
#include <Common/Utils/Pointers/Pointers.h>
#include <AzurelitEngine/GraphicsEngine/Interface/IGraphicsInterface.h>

class SceneManager;
class IGraphicsInterface;

class D3D12Renderer : public ISceneRenderer
{
public:
	D3D12Renderer();
	~D3D12Renderer();

	virtual bool Initialize() override;
	virtual bool RenderActiveScenes() override;
	virtual void Terminate()override;

	StrongPointer<IGraphicsInterface> graphicsInterface;

	StrongPointer<SceneManager> sceneManager;
};

