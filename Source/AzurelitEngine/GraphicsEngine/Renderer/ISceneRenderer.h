#pragma once

#include <Common/Utils/Singleton/ASingleton.h>

class ISceneRenderer
{
public:
	ISceneRenderer();
	~ISceneRenderer();

	virtual bool Initialize()			= 0;
	virtual bool RenderActiveScenes()	= 0;
	virtual void Terminate()			= 0;

};

