#pragma once

#include <AzurelitEngine/GraphicsEngine/Interface/IGraphicsInterface.h>

class D3D12GraphicsInterface :	public IGraphicsInterface
{
public:
	D3D12GraphicsInterface() = default;
	~D3D12GraphicsInterface() override;

	D3D12GraphicsInterface(D3D12GraphicsInterface&& other) = delete;
	D3D12GraphicsInterface(const D3D12GraphicsInterface& other) = delete;

	D3D12GraphicsInterface& operator=(D3D12GraphicsInterface&& other) = delete;
	D3D12GraphicsInterface& operator=(const D3D12GraphicsInterface& other) = delete;

	virtual bool Initialize() override;
	virtual void Terminate() override;



};

