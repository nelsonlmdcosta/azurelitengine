#pragma once

class IGraphicsInterface
{
public:
	IGraphicsInterface() = default;
	virtual ~IGraphicsInterface() {}

	IGraphicsInterface(IGraphicsInterface&& other) = delete;
	IGraphicsInterface(const IGraphicsInterface& other) = delete;

	IGraphicsInterface& operator=(IGraphicsInterface&& other) = delete;
	IGraphicsInterface& operator=(const IGraphicsInterface& other) = delete;

	virtual bool Initialize() = 0;
	virtual void Terminate() = 0;

	//StrongPointer<Mesh> CreateGFXMesh();
	//StringPointer<Shader> CreateGFXShader();
	//StrongPointer<Texture> CreateGFXTexture();
};