#pragma once

#include <AzurelitEngine\GraphicsEngine\Interface\IGraphicsInterface.h>
#include <AzurelitEngine\GraphicsEngine\Interface\D3D12GraphicsInterface\D3D12GraphicsInterface.h>
#include <AzurelitEngine\GraphicsEngine\Renderer\ISceneRenderer.h>
#include <AzurelitEngine\GraphicsEngine\Renderer\D3D12Renderer\D3D12Renderer.h>

#include <Common\Utils\Pointers\Pointers.h>

class GraphicsFactory
{
public:
	static StrongPointer<IGraphicsInterface> CreateGraphicsInterface()
	{
		// Find API Define And Create
		return ManagedPointers::MakeStrongPointer<D3D12GraphicsInterface>().As<IGraphicsInterface>();

	}

	static StrongPointer<ISceneRenderer> CreateGraphicsRenderer()
	{
		// Needs upgrading to a specific renderer as opposed to being a interface
		return ManagedPointers::MakeStrongPointer<D3D12Renderer>().As<ISceneRenderer>();
	}
};

