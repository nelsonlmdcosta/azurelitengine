// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

#include "AzurelitDemo.h"

#include <Common\Utils\Logging\AzurelitLogger.h>
#include <Common\Utils\BenchmarkingTimer\Timer.h>

#include <Common\Utils\DataStructures\ArrayList\ArrayList.h>

#include <Common\Utils\DataStructures\Queue\Queue.h>
#include <Common\Utils\DataStructures\Operations\Operations.h>

#include <Common\ThirdParty\RapidXML\rapidxml.hpp>
#include <queue>
#include <type_traits>
#include <AzurelitEngine\AzurelitCore\JobSystem\JobSystem.h>
#include <AzurelitEngine\AzurelitCore\JobSystem\Jobs\AJob.h>
#include <AzurelitEngine\AzurelitCore\JobSystem\Jobs\SceneLoadingJob.h>


// This Needs Defining From The Application We Are Creating
StrongPointer<AApplication> AApplication::applicationInstance = ManagedPointers::MakeStrongPointer<AzurelitDemo>().As<AApplication>();

AzurelitDemo::AzurelitDemo()
{

}

AzurelitDemo::~AzurelitDemo()
{

}

class TestClassForStuff 
{
public:
	TestClassForStuff() { }
	TestClassForStuff(std::string string) : i(0) { wow = string; }
	TestClassForStuff(int j) { i = j; }
	TestClassForStuff(const TestClassForStuff& other) { i = other.i; }
	TestClassForStuff(TestClassForStuff&& other) noexcept { i = other.i; other.i = 0; }

	TestClassForStuff& operator=(const TestClassForStuff& other) 
	{
		i = other.i;  return *this; 
	}
	TestClassForStuff& operator=(TestClassForStuff&& other) 
	{
		i = other.i; other.i = -1;  return *this;
	}


	~TestClassForStuff() {}

	bool operator<(const TestClassForStuff& rhs) const
	{
		return i < rhs.i;
	}

	bool operator==(const TestClassForStuff& rhs) const
	{
		return i == rhs.i;
	}

	int i = 0;
	std::string wow;
};

bool AzurelitDemo::InitializeSystems()
{
	AzurelitLogger::LogMessage("AzurelitDemo::IntitialzeSystems: Creating Required System");

	//int iterations = 100000;
	//PriorityQueueTest(iterations);
	//PriorityQueueStrongPointerTest(iterations);

	//UnsortedArrayListTest(iterations);
	//UnsortedArrayListStrongPointerTest(iterations);

	//SortedArrayListTest(iterations);
	//SortedArrayListStrongPointerTest(iterations);

	ThreadedJobSystemTest();

	return true;
}

void AzurelitDemo::TerminateSystems() 
{
	AzurelitLogger::LogMessage("AzurelitDemo::IntitialzeSystems: Terminating System");

	;
}

void AzurelitDemo::PriorityQueueTest(int iterations)
{
	AzurelitUtils::PriorityQueue<TestClassForStuff> priorityQueue;

	AzurelitLogger::LogMessage("Priority Queue: Push");
	{
		AzurelitUtils::BenchmarkTimer::ScopedTimer timer(AzurelitUtils::BenchmarkTimer::ScopedTimer::DisplayTimeAs::MicroSeconds);
		for (int i = 0; i < iterations; i++)
		{
			priorityQueue.Push(TestClassForStuff(i));
		}
	}

	AzurelitLogger::LogMessage("Priority Queue: Push");
	{
		AzurelitUtils::BenchmarkTimer::ScopedTimer timer(AzurelitUtils::BenchmarkTimer::ScopedTimer::DisplayTimeAs::MicroSeconds);
		for (int i = 0; i < iterations; i++)
		{
			priorityQueue.Pop();
		}
	}
}

void AzurelitDemo::PriorityQueueStrongPointerTest(int iterations)
{
	AzurelitUtils::PriorityQueue<StrongPointer<TestClassForStuff>> priorityQueue;

	AzurelitLogger::LogMessage("Priority Queue: Push Strong Pointer");
	{
		AzurelitUtils::BenchmarkTimer::ScopedTimer timer(AzurelitUtils::BenchmarkTimer::ScopedTimer::DisplayTimeAs::MicroSeconds);
		for (int i = 0; i < iterations; i++)
		{
			priorityQueue.Push(ManagedPointers::MakeStrongPointer<TestClassForStuff>(i));
		}
	}

	AzurelitLogger::LogMessage("Priority Queue: Pop Strong Pointer");
	{
		AzurelitUtils::BenchmarkTimer::ScopedTimer timer(AzurelitUtils::BenchmarkTimer::ScopedTimer::DisplayTimeAs::MicroSeconds);
		for (int i = 0; i < iterations; i++)
		{
			priorityQueue.Pop();
		}
	}
}

void AzurelitDemo::UnsortedArrayListTest(int iterations)
{
	AzurelitLogger::LogMessage("UnsortedArrayList: Adding Via New Element");

	AzurelitUtils::UnsortedArrayList<TestClassForStuff> vector(iterations);

	AzurelitUtils::BenchmarkTimer::ScopedTimer timer(AzurelitUtils::BenchmarkTimer::ScopedTimer::DisplayTimeAs::MicroSeconds);
	for (int i = 0; i < iterations; i++)
	{
		vector.AddNewElement(i);
	}
}

void AzurelitDemo::UnsortedArrayListStrongPointerTest(int iterations)
{
	AzurelitLogger::LogMessage("UnsortedArrayList: Adding Via Move");

	AzurelitUtils::UnsortedArrayList<StrongPointer<TestClassForStuff>> vector(iterations);
 	
	AzurelitUtils::BenchmarkTimer::ScopedTimer timer(AzurelitUtils::BenchmarkTimer::ScopedTimer::DisplayTimeAs::MicroSeconds);
 	for (int i = 0; i < iterations; i++)
 	{
 		vector.AddViaMove(ManagedPointers::MakeStrongPointer<TestClassForStuff, std::string>(std::to_string(i)));
 	}
 }

void AzurelitDemo::SortedArrayListTest(int iterations)
{
	AzurelitLogger::LogMessage("SortedArrayList: Adding Strong Pointers Via Copy");

	AzurelitUtils::SortedArrayList<TestClassForStuff> vector(iterations);

	AzurelitUtils::BenchmarkTimer::ScopedTimer timer(AzurelitUtils::BenchmarkTimer::ScopedTimer::DisplayTimeAs::MicroSeconds);
	for (int i = 0; i < iterations; i++)
	{
		vector.AddViaCopy(TestClassForStuff(i));
	}
}

void AzurelitDemo::SortedArrayListStrongPointerTest(int iterations)
{
	AzurelitLogger::LogMessage("SortedArrayList: Adding Strong Pointers Via Move");

	AzurelitUtils::SortedArrayList<StrongPointer<TestClassForStuff>> vector(iterations);

	AzurelitUtils::BenchmarkTimer::ScopedTimer timer(AzurelitUtils::BenchmarkTimer::ScopedTimer::DisplayTimeAs::MicroSeconds);
	for (int i = 0; i < iterations; i++)
	{
		vector.AddViaMove(ManagedPointers::MakeStrongPointer<TestClassForStuff, int>(i));
	}
}

void AzurelitDemo::ThreadedJobSystemTest()
{
	SceneLoadingJobData jobData1(1);
	StrongPointer<AJob> job1 = JobSystem::Instance().QueueNewJob<SceneLoadingJob, SceneLoadingJobData&>(jobData1);
	SceneLoadingJobData jobData2(2);
	StrongPointer<AJob> job2 = JobSystem::Instance().QueueNewJob<SceneLoadingJob, SceneLoadingJobData&>(jobData1);
	SceneLoadingJobData jobData3(3);
	StrongPointer<AJob> job3 = JobSystem::Instance().QueueNewJob<SceneLoadingJob, SceneLoadingJobData&>(jobData1);
	SceneLoadingJobData jobData4(4);
	StrongPointer<AJob> job4 = JobSystem::Instance().QueueNewJob<SceneLoadingJob, SceneLoadingJobData&>(jobData1);
	SceneLoadingJobData jobData5(5);
	StrongPointer<AJob> job5 = JobSystem::Instance().QueueNewJob<SceneLoadingJob, SceneLoadingJobData&>(jobData1);

	while
		(
			!job1->IsFinished()  ||
			!job2->IsFinished()  ||
			!job3->IsFinished()  ||
			!job4->IsFinished()  ||
			!job5->IsFinished() 
		)
	{
		StrongPointer<JobSystem> jobSystem = JobSystem::InstanceAsStrongPointer();
		int i = 0; ++i;
	}

	AzurelitLogger::LogMessage("All Jobs Complete!");
}
