/*
Author:   Nelson Luis Moreira da Costa
Email:    nelsonlmdcosta@gmail.com
LinkedIn: http://www.linkedin.com/in/nelsonlmdcosta

Brief:
Object That Helps Initialize Custom Systems and Levels For Demonstration Purposes, Also used to create a new game project
*/

#pragma once

#include <AzurelitEngine\AzurelitCore\Application\AApplication.h>

class AzurelitDemo : public AApplication
{
public:
	AzurelitDemo();
	~AzurelitDemo();

protected:

	virtual bool InitializeSystems() override;
	virtual void TerminateSystems()  override;

private:
	void PriorityQueueTest(int iterations);
	void PriorityQueueStrongPointerTest(int iterations);

	void UnsortedArrayListTest(int iterations);
	void UnsortedArrayListStrongPointerTest(int iterations);

	void SortedArrayListTest(int iterations);
	void SortedArrayListStrongPointerTest(int iterations);

	void ThreadedJobSystemTest();
};

