#pragma once

namespace AzurelitUtils
{
	namespace Comparitor
	{
		template<class Type = void>
		struct LessThan
		{
			constexpr bool operator()(const Type& lhs, const Type& rhs) const
			{
				return (lhs < rhs);
			}
		};

		template<class Type = void>
		struct LessThanOrEquals
		{
			constexpr bool operator()(const Type& lhs, const Type& rhs) const
			{
				return (lhs <= rhs);
			}
		};

		template<class Type = void>
		struct Equals
		{
			constexpr bool operator()(const Type& lhs, const Type& rhs) const
			{
				return (lhs == rhs);
			}
		};

		template<class Type = void>
		struct NotEquals
		{
			constexpr bool operator()(const Type& lhs, const Type& rhs) const
			{
				return (lhs != rhs);
			}
		};

		template<class Type = void>
		struct GreaterThan
		{
			constexpr bool operator()(const Type& lhs, const Type& rhs) const
			{
				return (lhs > rhs);
			}
		};

		template<class Type = void>
		struct GreaterThanOrEquals
		{
			constexpr bool operator()(const Type& lhs, const Type& rhs) const
			{
				return (lhs >= rhs);
			}
		};
	}
}