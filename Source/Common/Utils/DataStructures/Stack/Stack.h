#pragma once

#include <stack>

// TODO: Use A Custom Allocator To Reserve Some memory
// TODO: Make This Thread Safe

namespace AzurelitUtils
{
	template<typename T>
	class Stack
	{
	public:
		Stack() {}
		~Stack() 
		{
			Clear();
		}

		// Push Copy Object
		void Push(const T& item) 
		{
			internalStack.push(item); 
		}

		// Push Move Object
		void Push(T&& item) 
		{
			internalStack.push(item); 
		}

		// This will outright clean the object
		void Pop()
		{
			internalStack.pop();
		}

		// Count
		unsigned int Count() 
		{
			return (unsigned int)internalStack.size(); 
		}

		// If it has the IInitialiseTerminate Call Those?
		void IsEmpty() 
		{
			internalStack.empty();
		}

		void Clear()
		{
			int size = Count();
			for (int i = 0; i < size; ++i)
			{
				// Deinitialize?

				Pop();
			}
		}

		T& Top()
		{
			return internalStack.top();
		}

	private:
		std::stack<T> internalStack;
	};
}
