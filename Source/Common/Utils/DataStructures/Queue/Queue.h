#pragma once

#include <queue>
#include <Common\Utils\DataStructures\Operations\Operations.h>

// TODO: Use A Custom Allocator To Reserve Some memory
// TODO: Make This Thread Safe
// TODO: Complete this down the line

namespace AzurelitUtils
{
	// Brief: FIFO Queue First Element Is Always The Element That Is To Be Poped First
	template<typename T>
	class Queue
	{
	public:
		Queue()
		{}

		~Queue()
		{
			Clear();
		}

		// Push Copy Object
		void Push(const T&& item)
		{
			internalQueue.push(item);
		}

		// Push Move Object
		void Pop()
		{
			internalQueue.pop();
		}
		
		// Count
		unsigned int Count() const
		{
			return (unsigned int)internalQueue.size();
		}

		// If it has the IInitialiseTerminate Call Those?
		void IsEmpty()
		{
			internalQueue.empty();
		}

		void Clear()
		{
			int size = Count();
			for (int i = 0; i < size; ++i)
			{
				// Deinitialize?

				Pop();
			}
		}

		T& Front()
		{
			return internalQueue.front();
		}

	private:
		std::queue<T> internalQueue;
	};

	// Priority Queue Wrapper of STD, defaults to less than comparitor
	// NOTE: The class that is put in this container MUST overload the requested operator
	//		 So lessthan will require < operator etc etc
	// NOTE: This works as intended, the value related to you comparitor will always be at the top after poping
	//		 So This is NOT a sorted array list
	template<class T, typename Comparitor = AzurelitUtils::Comparitor::LessThan<T>>
	class PriorityQueue
	{
	public:
		explicit PriorityQueue()
		{
			internalPriorityQueue = std::priority_queue<T, std::vector<T>, Comparitor>();
		}

		~PriorityQueue()
		{
			Clear();
		}

		// Push Copy Object
		void Push(const T& item)
		{
			internalPriorityQueue.push(item);
		}
		
		void Push(T&& item)
		{
			internalPriorityQueue.push((T&&)item);
		}

		// Push Move Object
		void Pop()
		{
			internalPriorityQueue.pop();
		}

		const T& Top()
		{
			return internalPriorityQueue.top();
		}

// 		const T& GetItemAtIndex(int i)
// 		{
// 			return internalPriorityQueue[i];
// 		}

		// Count
		int Count()
		{
			return (int)internalPriorityQueue.size();
		}

		// If it has the IInitialiseTerminate Call Those?
		void IsEmpty()
		{
			internalPriorityQueue.empty();
		}

		void Clear()
		{
			int size = Count();
			for (int i = 0; i < size; ++i)
			{
				// Deinitialize?

				Pop();
			}
		}

	private:
		std::priority_queue<T, std::vector<T>, Comparitor> internalPriorityQueue;
		Comparitor comparitorOperator;
	};
}