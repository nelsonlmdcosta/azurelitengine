#pragma once
#include <vector>

#include <Common\Utils\DataStructures\Operations\Operations.h>
#include <Common\Utils\Pointers\Pointers.h>
#include <Common\Utils\Logging\AzurelitLogger.h>

// UnsortedArray
// SortedArray

// Wrappers around STD::Vector for now, later down the line we might consider implementing EASTL

namespace AzurelitUtils
{
	// Make sure you use noexcept at the end of your move constructor on the object so that this implicitly uses that when expanding
	template<typename T>
	class UnsortedArrayList
	{
	public:
		UnsortedArrayList(unsigned int initialCapacity = 0)
		{
			Reserve(initialCapacity);
		}

		// Fastest of the three add functions
		template<typename... Args>
		T& AddNewElement(Args... constructorArguments)
		{
			return internalVector.emplace_back(constructorArguments...);
		}

		void AddViaCopy(const T& item)
		{
			internalVector.push_back(item);
		}

		void AddViaMove(T&& item)
		{
			internalVector.push_back(std::move(item));
		}

		// Removes are Costly as they shift ALL THE ELEMENTS causing lots of copy constructions
		// TODO: Return iterator
		void Remove(const T& item)
		{
			auto itemFoundAt = FindPositionFromItem(item);

			// Is the element the last item?
			if (itemFoundAt != *internalVector.back())
			{
				// Switch element with the back then clear the back element
				*itemFoundAt = std::move(internalVector.back());
			}

			internalVector.erase(internalVector.back());
		}

		// Removes are Costly as they shift ALL THE ELEMENTS causing lots of copy constructions
		// TODO: Switch last and the element around?
		// TOOD: index out range?
		void RemoveAt(unsigned int index)
		{
			auto itemAt = internalVector.begin() + index;
			auto backItem = internalVector.end() - 1;
			if ( itemAt != backItem)
			{
				*itemAt = std::move(*backItem);
			}

			internalVector.erase(internalVector.end() - 1);
		}

		auto FindPositionFromItem(const T& item)
		{
			unsigned int length = (unsigned int)internalVector.size();
			for (auto iterator = internalVector.begin(); iterator != internalVector.end(); ++internalVector)
			{
				if (*iterator == item)
				{
					return iterator;
				}
			}
		}

		unsigned int Count()
		{
			return (unsigned int)internalVector.size();
		}

		bool IsEmpty()
		{
			return Count() > 0;
		}

		void Clear()
		{
			internalVector.clear();
		}

		// Resizes the vector and the allocated memory is set to defaults
		// Useful for when you want to allocate a predetermined block and initialize them separately
		void Resize(unsigned int resizeCapacity)
		{
			internalVector.resize(resizeCapacity);
		}

		void Reserve(unsigned int reserveCapacity)
		{
			internalVector.reserve(reserveCapacity);
		}

		bool IsValidIndex(unsigned int index)
		{
			return index < internalVector.size() ? true : false;
		}

		T& operator[](unsigned int index)
		{
			if (!IsValidIndex(index))
			{
				AzurelitLogger::LogCriticalError("UnsortedArrayList::operator[], unsafe index passed in returning time to crash!");
			}
			return internalVector[index];
		}

	private:
		std::vector<T> internalVector;
	};

	// Make sure you use noexcept at the end of your move constructor on the object so that this implicitly uses that when expanding
	template<typename T, typename Comparitor = AzurelitUtils::Comparitor::LessThan<T>>
	class SortedArrayList
	{
	public:

		SortedArrayList(unsigned int initialSize = 0)
		{
			internalVector.reserve(initialSize);
		}

		void AddViaCopy(const T& item)
		{
			internalVector.insert
			(
				// Always insert at the upper bound, this way it shifts less elements
				std::upper_bound(internalVector.begin(), internalVector.end(), item, Comparitor()),
				item
			);
		}

		void AddViaMove(T&& item)
		{
			internalVector.insert
			(
				// Always insert at the upper bound, this way it shifts less elements
				std::upper_bound(internalVector.begin(), internalVector.end(), item, Comparitor()),
				std::move(item)
			);
		}

		// Removes are Costly as they shift ALL THE ELEMENTS causing lots of copy constructions, necessary evil for SortedVectors
		void Remove(const T& item)
		{
			internalVector.erase(FindPositionFromItem(item));
		}

		// Removes are Costly as they shift ALL THE ELEMENTS causing lots of copy constructions, necessary evil for SortedVectors
		void RemoveAt(unsigned int index)
		{
			internalVector.erase(internalVector.begin() + index);
		}

		auto FindPositionFromItem(const T& item)
		{
			unsigned int length = (unsigned int)internalVector.size();
			for (auto iterator = internalVector.begin(); iterator != internalVector.end(); ++internalVector)
			{
				if (*iterator == item)
				{
					return iterator;
				}
			}
		}

		unsigned int Count()
		{
			return (unsigned int)internalVector.size();
		}

		bool IsEmpty()
		{
			return Count() > 0;
		}

		void Clear()
		{
			internalVector.size();
		}

		bool IsValidIndex(unsigned int index)
		{
			return index < internalVector.size() ? true : false;
		}

	private:
		std::vector<T> internalVector;

	};

	template<typename T, size_t size = 10>
	class Array
	{
	public:

	private:
		std::array<T, size> internalArray;
	};
}