#pragma once

#include <map>

namespace AzurelitUtils
{
	template<T>
	class Map
	{
		Map() = default;
		~Map() = default;

		Map(Map&& other) = delete;
		Map(const Map& other) = delete;

		Map& operator=(Map& other) = delete;
		Map& operator=(const Map& other) = delete;

		void Insert( T& element, );
		void Remove();
		void Find();

	private:
		std::map<HashedKey, T> internalMap;
	};

	template<T>
	class MultiMap
	{
		MultiMap() = default;
		~MultiMap() = default;

		MultiMap(MultiMap&& other) = delete;
		MultiMap(const MultiMap& other) = delete;

		MultiMap& operator=(MultiMap& other) = delete;
		MultiMap& operator=(const MultiMap& other) = delete;


	private:
		std::multimap<T> internalMap;
	};

	template<T>
	class HashMap
	{
		HashMap() = default;
		~HashMap() = default;

		HashMap(HashMap&& other) = delete;
		HashMap(const HashMap& other) = delete;

		HashMap& operator=(HashMap& other) = delete;
		HashMap& operator=(const HashMap& other) = delete;


	private:
		std::multimap<T> internalMap;
	};
}

