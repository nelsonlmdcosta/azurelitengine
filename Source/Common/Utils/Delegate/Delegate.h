/*
Author:   Nelson Luis Moreira da Costa
Email:    nelsonlmdcosta@gmail.com
LinkedIn: http://www.linkedin.com/in/nelsonlmdcosta

Brief:
Delegate Wraps a Delegate Stub Object That Cooks The Typing Information We Need To Call Through The Lambda

TODO:
// TODO: Memory management, allowing and disallowing delegate move and copy operations, need to see what is worth keeping and delete the rest so wrong/old information isnt passed around
// TODO: Error Logging
// TODO: Add some debug information too? in case of errors to make it easier to find problems, like class names etc
*/

#pragma once

#include <functional>


template <typename TReturn, typename... Args>
class Delegate
{
public:
	Delegate() {}
	Delegate(const Delegate& other)
		: delegateStub(other.delegateStub)
	{}
	Delegate(Delegate&& other)
		: delegateStub(other.delegateStub)
	{}

	Delegate& operator=(const Delegate& other)	= delete;
	Delegate& operator=(Delegate&& other)		= delete;

	template<class Type, TReturn(Type::*funcPtr)(Args...)>
	bool Register(Type* inst)
	{
		if (!IsRegistered())
		{
			auto CallMemberFunction = [](void* component, Args... args)
			{
				Type* typeInst = (Type*)component;
				(typeInst->*funcPtr)(args...);
			};

			delegateStub.m_objectPtr = inst;
			delegateStub.m_function = CallMemberFunction;

			return true;
		}

		return false;
	}

	bool Unregister()
	{
		delegateStub.Clear();
	}

	bool IsRegistered()
	{
		return delegateStub.m_objectPtr != nullptr;
	}

	bool Is(void* object)
	{
		return delegateStub.m_objectPtr == object;
	}

	TReturn operator()(Args... args)
	{
		delegateStub.Call(args...);
	}

private:

	struct DelegateStub
	{
		DelegateStub()
			: m_objectPtr(nullptr)
			, m_function(nullptr)
		{}

		void* m_objectPtr;
		std::function<TReturn(void*, Args...)> m_function;

		void Call(Args... args)
		{ 
			if (m_function && m_objectPtr)
			{
				m_function(m_objectPtr, args...);
			}
		}

		void Clear()
		{
			m_objectPtr = nullptr;
			m_function = nullptr;
		}
	};

	DelegateStub delegateStub; // <-- This Is Where The information is stored, an event uses multiple delegates, a delegate holds a function. a callback will use a delegate.
};
