#pragma once

#include <memory>
#include <type_traits>

template<typename T>
class WeakPointer;

template<typename T>
class StrongPointer;

namespace ManagedPointers
{
	template<typename ClassInstance, typename... ConstructionArguments>
	static StrongPointer<ClassInstance> MakeStrongPointer(ConstructionArguments... args)
	{
		return std::make_shared<ClassInstance, ConstructionArguments...>(std::forward<ConstructionArguments>(args)...);
	}

	// Forward

	// Move
}

template<typename T>
class WeakPointer
{
	friend class StrongPointer<T>;

public:
	WeakPointer()
	{
		weakPointer.reset();
	}

	~WeakPointer()
	{
		weakPointer.reset();
	}

	// ConversionConstructors
	WeakPointer(const WeakPointer<T>& other)
	{
		weakPointer = other->weakPointer;
	}
	
	WeakPointer(const StrongPointer<T>& sharedPointer)
	{
		weakPointer = sharedPointer.strongPointer;
	}

	WeakPointer<T> operator=(const StrongPointer<T>& sharedPointer)
	{
		weakPointer = sharedPointer;
	}

	// Done this way to remove that annoying lock call and return a thread safe object
	// Returns shares so you can complete your execution without the object dissapearing into the ether halfway through a threads execution
	std::shared_ptr<T> operator->()
	{
		return weakPointer.lock();
	}

	bool IsValid() const
	{
		return !weakPointer.expired();
	}

	void Clear()
	{
		weakPointer.reset();
	}

	template<typename TypeToConvertTo>
	WeakPointer<TypeToConvertTo> As()
	{
		return std::dynamic_pointer_cast<TypeToConvertTo>(weakPointer);
	}

	bool Equals(const WeakPointer<T>& rhs)
	{
		if (!IsValid() || !rhs.IsValid())
			return false;

		return (GetAsRawPointer() == rhs.GetAsRawPointer());
	}

	T* GetAsRawPointer()
	{
		return weakPointer.get();
	}

	const T& GetAsReference() const
	{
		return *weakPointer;
	}

	std::weak_ptr<T> weakPointer;
};

template<typename T>
class StrongPointer
{
	friend class WeakPointer<T>;

public: // Constructors Destructors and Converter

	StrongPointer()
	{
		strongPointer.reset();
	}

	~StrongPointer()
	{
		strongPointer.reset();
	}

	StrongPointer(const StrongPointer<T>& other)
	{
		strongPointer = other.strongPointer;
	}

	StrongPointer(StrongPointer<T>&& other)
	{
		strongPointer = other.strongPointer;

		other.strongPointer = nullptr;
	}

	StrongPointer<T> operator=(const StrongPointer<T>& other)
	{
		strongPointer = other.strongPointer;
		return *this;
	}

	StrongPointer<T> operator=(StrongPointer<T>&& other)
	{
		strongPointer = other.strongPointer;
		other.strongPointer = nullptr;
		return *this;
	}

	StrongPointer(const std::shared_ptr<T>& other)
	{
		strongPointer = other;
	}

public: // Override Operators

	// Done this way to remove that annoying lock call and return a thread safe object
	// Returns a copy so you can complete your execution without the object dissapearing into the ether halfway through thread execution
	std::shared_ptr<T> operator->()
	{
		return strongPointer;
	}

	StrongPointer<T> operator=(const std::shared_ptr<T>& sharedPointer)
	{
		strongPointer = sharedPointer;
	}

public: // Helpers

	bool IsValid() const
	{
		return strongPointer != nullptr;
	}

	void Clear()
	{
		strongPointer.reset();
	}

	template<typename TypeToConvertTo>
	StrongPointer<TypeToConvertTo> As()
	{
		return std::dynamic_pointer_cast<TypeToConvertTo>(strongPointer);
	}

	bool Equals(const StrongPointer<T>& rhs)
	{
		if (!IsValid() || !rhs.IsValid())
			return false;

		return (GetAsRawPointer() == rhs.GetAsRawPointer());
	}

	T* GetAsRawPointer()
	{
		return strongPointer.get();
	}

	T& GetAsReference() const
	{
		return *strongPointer;
	}

private:

	std::shared_ptr<T> strongPointer;
};

// These are here for compatibility with comparator objects (so we can sort these objects in SortedArrayLists And PriorityQueue's)

// Strong Pointer Version

template<typename T>
bool operator<(const StrongPointer<T>& lhs, const StrongPointer<T>& rhs) noexcept
{
	if (!lhs.IsValid() || !rhs.IsValid())
		return false;

	return (lhs.GetAsReference() < rhs.GetAsReference());
}

template<typename T>
bool operator<=(const StrongPointer<T>& lhs, const StrongPointer<T>& rhs) noexcept
{
	if (!lhs.IsValid() || !rhs.IsValid())
		return false;

	return (lhs.GetAsReference() <= rhs.GetAsReference());
}

template<typename T>
bool operator==(const StrongPointer<T>& lhs, const StrongPointer<T>& rhs) noexcept
{
	if (!lhs.IsValid() || !rhs.IsValid())
		return false;

	return (lhs.GetAsReference() == rhs.GetAsReference());
}

template<typename T>
bool operator!=(const StrongPointer<T>& lhs, const StrongPointer<T>& rhs) noexcept
{
	if (!lhs.IsValid() || !rhs.IsValid())
		return false;

	return (lhs.GetAsReference() != rhs.GetAsReference());
}

template<typename T>
bool operator>=(const StrongPointer<T>& lhs, const StrongPointer<T>& rhs) noexcept
{
	if (!lhs.IsValid() || !rhs.IsValid())
		return false;

	return (lhs.GetAsReference() >= rhs.GetAsReference());
}

template<typename T>
bool operator>(const StrongPointer<T>& lhs, const StrongPointer<T>& rhs) noexcept
{
	if (!lhs.IsValid() || !rhs.IsValid())
		return false;

	return (lhs.GetAsReference() > rhs.GetAsReference());
}

// Weak Pointer Version

template<typename T>
bool operator<(const WeakPointer<T>& lhs, const WeakPointer<T>& rhs) noexcept
{
	if (!lhs.IsValid() || !rhs.IsValid())
		return false;

	return (lhs.GetAsReference() < rhs.GetAsReference());
}

template<typename T>
bool operator<=(const WeakPointer<T>& lhs, const WeakPointer<T>& rhs) noexcept
{
	if (!lhs.IsValid() || !rhs.IsValid())
		return false;

	return (lhs.GetAsReference() <= rhs.GetAsReference());
}

template<typename T>
bool operator==(const WeakPointer<T>& lhs, const WeakPointer<T>& rhs) noexcept
{
	if (!lhs.IsValid() || !rhs.IsValid())
		return false;

	return (lhs.GetAsReference() == rhs.GetAsReference());
}

template<typename T>
bool operator!=(const WeakPointer<T>& lhs, const WeakPointer<T>& rhs) noexcept
{
	if (!lhs.IsValid() || !rhs.IsValid())
		return false;

	return (lhs.GetAsReference() != rhs.GetAsReference());
}

template<typename T>
bool operator>=(const WeakPointer<T>& lhs, const WeakPointer<T>& rhs) noexcept
{
	if (!lhs.IsValid() || !rhs.IsValid())
		return false;

	return (lhs.GetAsReference() >= rhs.GetAsReference());
}

template<typename T>
bool operator>(const WeakPointer<T>& lhs, const WeakPointer<T>& rhs) noexcept
{
	if (!lhs.IsValid() || !rhs.IsValid())
		return false;

	return (lhs.GetAsReference() > rhs.GetAsReference());
}
