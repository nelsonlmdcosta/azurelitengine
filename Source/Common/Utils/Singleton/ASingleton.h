/*
Author:   Nelson Luis Moreira da Costa
Email:    nelsonlmdcosta@gmail.com
LinkedIn: http://www.linkedin.com/in/nelsonlmdcosta

Description:
Base Class For The Singleton Design Pattern For Various Systems
Lifetime of the object is when it's requested for the first time till the end of the application lifecycle
*/

#pragma once

#include <Common\Utils\Pointers\Pointers.h>

template <typename T>
class ASingleton
{
protected:

	ASingleton() {}
	~ASingleton() {}

public:

	// Disable Copy and Move Constructors and Operators
	ASingleton(ASingleton&& instance)			= delete;
	ASingleton(const ASingleton& instance)		= delete;
	T& operator=(ASingleton<T>&& instance)		= delete;
	T& operator=(const ASingleton<T>& instance) = delete;

	// Get The Singleton Instance
	static T& Instance()
	{
		// Static declaration in here so it's created when called and cleaned up at the end
		//static T singletonInstance;

		return singletonInstance.GetAsReference();
	}

	static StrongPointer<T> InstanceAsStrongPointer()
	{
		return singletonInstance;
	}

	static StrongPointer<T> singletonInstance;
};

template<typename T>
 StrongPointer<T> ASingleton<T>::singletonInstance = ManagedPointers::MakeStrongPointer<T>();
