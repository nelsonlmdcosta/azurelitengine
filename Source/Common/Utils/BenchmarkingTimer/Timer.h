#pragma once
#include <chrono>

// TODO: This should in theory either print or write out the value to a variable that is apssed in as a construction argument
// TODO: Remove all the crappy type casts as well
namespace AzurelitUtils
{
	namespace BenchmarkTimer
	{
		// Timer Class Mostly For Scoped Code
		class ScopedTimer
		{
		public:

			enum DisplayTimeAs
			{
				Seconds,
				MilliSeconds,
				MicroSeconds,
				NanoSeconds
			};

			ScopedTimer(DisplayTimeAs displayAs = DisplayTimeAs::MilliSeconds, double* output = nullptr)
			{
				startTime = std::chrono::high_resolution_clock::now();

				displayType = displayAs;
				targetOutput = output;
			}
			
			~ScopedTimer()
			{
				Stop();

				CalculateDuration();

				DisplayOrWriteOutResults();
			}

		private:

			void Stop()
			{
				stopTime = std::chrono::high_resolution_clock::now();
			}

			void CalculateDuration()
			{
				duration = GetDurationFromStartAndEndPointsAs(displayType);
			}

			void DisplayOrWriteOutResults()
			{
				if (targetOutput)
				{
					(*targetOutput) = duration;
				}
				else
				{
					std::string outputString;
					GenerateOutputString(outputString);

					AzurelitLogger::LogMessage(outputString);
				}
			}

			double GetDurationFromStartAndEndPointsAs(DisplayTimeAs unit)
			{
				double curatedStartTime = 0.0f;
				double curatedStopTime = 0.0f;

				switch (unit)
				{
					case Seconds:
					{
						curatedStartTime = (double)
							std::chrono::time_point_cast<std::chrono::seconds>(startTime).time_since_epoch().count();

						curatedStopTime = (double)
							std::chrono::time_point_cast<std::chrono::seconds>(stopTime).time_since_epoch().count();
						
						break;
					}

					case MilliSeconds:
					{
						curatedStartTime = (double)
							std::chrono::time_point_cast<std::chrono::milliseconds>(startTime).time_since_epoch().count();

						curatedStopTime = (double)
							std::chrono::time_point_cast<std::chrono::milliseconds>(stopTime).time_since_epoch().count();

						break;
					}

					case MicroSeconds:
					{
						curatedStartTime = (double)
							std::chrono::time_point_cast<std::chrono::microseconds>(startTime).time_since_epoch().count();

						curatedStopTime = (double)
							std::chrono::time_point_cast<std::chrono::microseconds>(stopTime).time_since_epoch().count();

						break;
					}

					case NanoSeconds:
					{
						curatedStartTime = (double)
							std::chrono::time_point_cast<std::chrono::nanoseconds>(startTime).time_since_epoch().count();

						curatedStopTime = (double)
							std::chrono::time_point_cast<std::chrono::nanoseconds>(stopTime).time_since_epoch().count();

						break;
					}
				}

				return curatedStopTime - curatedStartTime;
			}

			void GenerateOutputString(std::string& output)
			{
				switch (displayType)
				{
					case Seconds:
					{
						output = std::to_string((long long)duration) + "s";

						break;
					}

					case MilliSeconds:
					{
						output = std::to_string((long long)duration) + "ms";

						break;
					}

					case MicroSeconds:
					{
						output = std::to_string((long long)duration) + "us";

						break;
					}

					case NanoSeconds:
					{
						output = std::to_string((long long)duration) + "ns";

						break;
					}
				}
			}

			DisplayTimeAs displayType = DisplayTimeAs::NanoSeconds;

			std::chrono::time_point< std::chrono::high_resolution_clock > startTime;
			std::chrono::time_point< std::chrono::high_resolution_clock > stopTime;

			double duration = 0.0f;

			double* targetOutput = nullptr;
		};
	}
}
