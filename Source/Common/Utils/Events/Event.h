/*
Author:   Nelson Luis Moreira da Costa
Email:    nelsonlmdcosta@gmail.com
LinkedIn: http://www.linkedin.com/in/nelsonlmdcosta

Brief:
Event that holds onto multiple delegate objects, broadcasts a signal to all

TODO:
// TODO: Memory management, allowing and disallowing delegate move and copy operations, need to see what is worth keeping and delete the rest so wrong/old information isnt passed around
// TODO: Error Logging
// TODO: Add some debug information too? in case of errors to make it easier to find problems, like class names etc
*/

#pragma once

#include <vector>

#include <Common\Utils\Delegate\Delegate.h>

template<typename... Args>
class Event
{
public:
	Event() 
		: eventDelegates(0)
	{}
	Event(const Event& other)
		:eventDelegates(other.eventDelegates)
	{}

	template<class Type, void(Type::*funcPtr)(Args...)>
	bool Subscribe(Type* instance)
	{
		Delegate<void, Args...> eventDelegate;
		if (eventDelegate.Register<Type, funcPtr>(instance))
		{
			eventDelegates.emplace_back(eventDelegate);
			return true;
		}
		return false;
	}

	template<class Type>
	bool Unsubscribe(Type* instance)
	{
		for (int i = 0; i < eventDelegates.size(); i++)
		{
			Delegate<void, Args...>& eventDelegate = eventDelegates[i];
			if (eventDelegate.Is(instance))
			{
				eventDelegates.erase(eventDelegates.begin() + i);

				return true;
			}
		}
		return false;
	}

	template<class Type>
	bool IsSubsribed(Type* instance)
	{
		for (int i = 0; i < eventDelegates.size(); i++)
		{
			if (eventDelegates[i].Is(instance))
			{
				return true;
			}
		}
		return false;
	}

	void BroadCast(Args... args)
	{
		for (int i = 0; i < eventDelegates.size(); i++)
		{
			eventDelegates[i](args...);
		}
	}

	void Clear()
	{
		eventDelegates.clear();
	}


private:
	std::vector<Delegate<void, Args...>> eventDelegates;
};
