#pragma once
#include <thread>

namespace AzurelitUtils
{
	class Thread
	{
	public:
		Thread();
		~Thread();

		Thread(const Thread& other)
		{
			thread = (std::thread&&)other.thread;// std::move(other.thread);
		}
		Thread& operator=(const Thread& other)
		{
			thread = (std::thread&&)other.thread;// std::move(other.thread);
			return *this;
		}

		template<typename TClass, void(TClass::*funcPtr)()>
		void BindFunction(TClass* typedObject)
		{
			thread = std::thread(funcPtr, typedObject);
		}
		
		void Join()
		{
			thread.join();
		}

		void Detach()
		{
			thread.detach();
		}

		unsigned int ThreadID() const
		{
			return 1;// (unsigned int)thread.get_id();
		}

	private:

		std::thread thread;
	};
}
