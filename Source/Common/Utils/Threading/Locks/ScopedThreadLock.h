#pragma once
#include <mutex>
#include <Common\Utils\Pointers\Pointers.h>

class ThreadLock
{
public:
	
	ThreadLock()
	{
		lockObject = ManagedPointers::MakeStrongPointer<std::mutex>();
	}

	~ThreadLock()
	{
		lockObject.Clear();
	}

	ThreadLock(const ThreadLock& other)
		: lockObject(other.lockObject)
	{}

	ThreadLock(ThreadLock&& other)
		: lockObject(other.lockObject)
	{}


	void Lock()
	{
		if (lockObject.IsValid())
		{
			lockObject->lock();
		}
	}

	bool TryLock()
	{
		if (lockObject.IsValid())
		{
			return lockObject->try_lock();
		}
		return false;
	}

	void Unlock()
	{
		if (lockObject.IsValid())
		{
			lockObject->unlock();
		}
	}

private:

	StrongPointer<std::mutex> lockObject;
};

class ScopedThreadLock
{
public:
	ScopedThreadLock() = delete;
	ScopedThreadLock(ThreadLock& lock)
		: threadLock(lock)
	{
		threadLock.Lock();
	}
	~ScopedThreadLock()
	{
		threadLock.Unlock();
	}

	ScopedThreadLock(const ThreadLock&) = delete;

	ScopedThreadLock(ThreadLock&&) = delete;
	ScopedThreadLock& operator=(ThreadLock&&) = delete;

private:
	ThreadLock& threadLock;
};