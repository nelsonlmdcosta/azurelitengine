// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
#include "AzurelitLogger.h"

#include <AzurelitEngine/AzurelitCore/Application/AApplication.h>

#include <string>
#include <ctime>
#include <iostream>
#include <sstream>

std::string AzurelitLogger::messageBuffer;

bool AzurelitLogger::InitializeLogger()
{
	messageBuffer.reserve(AzurelitLoggerBufferSize);

	return true;
}

void AzurelitLogger::LogMessage(std::string input)
{
	ConstructMessageBuffer("[Message] ", input);

	PrintMessageBuffer();
}

void AzurelitLogger::LogWarning(std::string input)
{
	ConstructMessageBuffer("[Warning] ", input);

	PrintMessageBuffer();
}

void AzurelitLogger::LogError(std::string input, bool forceQuit)
{
	ConstructMessageBuffer("[Error] ", input);

	PrintMessageBuffer();

	if (forceQuit)
	{
		AApplication::Instance()->RequestApplicationTermination();
	}
}

void AzurelitLogger::LogCriticalError(std::string input)
{
	ConstructMessageBuffer("[System Error] ", input);

	PrintMessageBuffer();

	AApplication::Instance()->RequestApplicationTermination();
}

void AzurelitLogger::ConstructMessageBuffer(std::string messageType, std::string& message)
{
	messageBuffer.clear();

	time_t now = time(0);
	tm lt;
	localtime_s(&lt, &now);

	std::stringstream formatedTime;
	formatedTime << "[" << std::to_string(lt.tm_hour) << ":" + std::to_string(lt.tm_min) << ":" << std::to_string(lt.tm_sec) << "]";
	
	messageBuffer += formatedTime.str();
	messageBuffer += messageType;
	messageBuffer += message;
}

void AzurelitLogger::PrintMessageBuffer()
{
	std::cout << messageBuffer.c_str() << std::endl << std::flush;
}
