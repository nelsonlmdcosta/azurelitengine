/*
Author:   Nelson Luis Moreira da Costa
Email:    nelsonlmdcosta@gmail.com
LinkedIn: http://www.linkedin.com/in/nelsonlmdcosta

Brief:
Static class that simplifies logging, might get substituted later on by spdlog
https://github.com/gabime/spdlog

TODO:
- Add Console Text Colouring
- Add pop up message on system error to stop the program just closing
- Add functionality to output to the cmd or file or both
*/

#pragma once

#include <string>

#define AzurelitLoggerBufferSize 100

class AzurelitLogger
{
public:

	AzurelitLogger() = delete;
	AzurelitLogger(const AzurelitLogger& other) = delete;
	AzurelitLogger(AzurelitLogger&& other) = delete;
	AzurelitLogger& operator=(const AzurelitLogger& other) = delete;
	AzurelitLogger& operator=(AzurelitLogger&& other) = delete;

	// Called From The Main Implementation To Reserve A Block Of Memory For The Message Buffer
	static bool InitializeLogger();

	// Logs A Message
	static void LogMessage(std::string input);

	// Logs A Warning
	static void LogWarning(std::string input);

	// Logs An Error, That We Can Choose To Close The Application Or Not
	static void LogError(std::string input, bool forceQuit = true);
	
	// Logs A System Based Error, Exits On Close
	static void LogCriticalError(std::string input);

private:
	// For internal use only, writes straight into message buffer
	static void ConstructMessageBuffer(std::string messageType, std::string& message);
	static void PrintMessageBuffer();

	static std::string messageBuffer;
};
